package az.ingress.ms23lesson4fruitapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ms23Lesson4FruitApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(Ms23Lesson4FruitApiApplication.class, args);
    }

}
