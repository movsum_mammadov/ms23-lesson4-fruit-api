package az.ingress.ms23lesson4fruitapi.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FruitResponseDto {

    private Long id;

    private String name;

    private String amount;

    private Double price;
}
