package az.ingress.ms23lesson4fruitapi.dto;


import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FruitRequestDto {

    @NotBlank
    private String name;

    @NotNull
    private String amount;

    @NotNull
    private Double price;

}
