package az.ingress.ms23lesson4fruitapi.rest;


import az.ingress.ms23lesson4fruitapi.dto.FruitRequestDto;
import az.ingress.ms23lesson4fruitapi.dto.FruitResponseDto;
import az.ingress.ms23lesson4fruitapi.service.FruitService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/fruits")
@RequiredArgsConstructor
public class FruitAPI {

    private final FruitService fruitService;

    @GetMapping
    public List<FruitResponseDto> list() {
        return fruitService.list();
    }

    @GetMapping("/pageable")
    public List<FruitResponseDto> listPageable(@RequestParam(defaultValue = "0")  Integer pageNumber,
                                               @RequestParam(defaultValue = "5")  Integer size) {
        return fruitService.listPageable(PageRequest.of(pageNumber,size));
    }

    @GetMapping("/{id}")
    public FruitResponseDto get(@PathVariable Long id){
        return fruitService.get(id);
    }

    @PostMapping
    public FruitResponseDto create(@Validated @RequestBody FruitRequestDto fruitRequestDto) {
        return fruitService.create(fruitRequestDto);
    }

    @PutMapping("/{id}")
    public FruitResponseDto update(@PathVariable Long id,
                                    @Validated @RequestBody FruitRequestDto fruitRequestDto){
        return fruitService.update(id,fruitRequestDto);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        fruitService.delete(id);
    }

}
