package az.ingress.ms23lesson4fruitapi.repository;

import az.ingress.ms23lesson4fruitapi.domain.FruitEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FruitRepository extends JpaRepository<FruitEntity,Long> {
}
