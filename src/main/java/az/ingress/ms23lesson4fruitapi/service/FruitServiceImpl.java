package az.ingress.ms23lesson4fruitapi.service;

import az.ingress.ms23lesson4fruitapi.domain.FruitEntity;
import az.ingress.ms23lesson4fruitapi.dto.FruitRequestDto;
import az.ingress.ms23lesson4fruitapi.dto.FruitResponseDto;
import az.ingress.ms23lesson4fruitapi.repository.FruitRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class FruitServiceImpl implements FruitService {

    private final FruitRepository fruitRepository;

    private final ModelMapper mapper;

    @Override
    public List<FruitResponseDto> list() {
        return fruitRepository.findAll()
                .stream()
                .map(fruitEntity -> mapper.map(fruitEntity, FruitResponseDto.class))
                .toList();
    }

    @Override
    public List<FruitResponseDto> listPageable(Pageable pageable) {
        return fruitRepository.findAll(pageable).get()
                .map(fruitEntity -> mapper.map(fruitEntity, FruitResponseDto.class))
                .toList();
    }

    @Override
    public FruitResponseDto get(Long id) {
        return fruitRepository.findById(id)
                .map(fruitEntity -> mapper.map(fruitEntity, FruitResponseDto.class))
                .orElseThrow(() -> new RuntimeException("Fruit with id %d not found".formatted(id)));

    }

    @Override
    public FruitResponseDto create(FruitRequestDto fruitRequestDto) {
        FruitEntity fruitEntity = mapper.map(fruitRequestDto, FruitEntity.class);
        return mapper.map(fruitRepository.save(fruitEntity), FruitResponseDto.class);
    }

    @Override
    public FruitResponseDto update(Long id, FruitRequestDto fruitRequestDto) {
        FruitEntity fruitEntity = mapper.map(fruitRequestDto, FruitEntity.class);
        fruitEntity.setId(id);
        fruitRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Fruit with id %d not found".formatted(id)));
        return mapper.map(fruitRepository.save(fruitEntity), FruitResponseDto.class);
    }

    @Override
    public void delete(Long id) {
        fruitRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Fruit with id %d not found".formatted(id)));
        fruitRepository.deleteById(id);
    }
}
