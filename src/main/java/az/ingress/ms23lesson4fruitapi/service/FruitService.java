package az.ingress.ms23lesson4fruitapi.service;

import az.ingress.ms23lesson4fruitapi.dto.FruitRequestDto;
import az.ingress.ms23lesson4fruitapi.dto.FruitResponseDto;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface FruitService {

    List<FruitResponseDto> list();

    List<FruitResponseDto> listPageable(Pageable pageable);

    FruitResponseDto get(Long id);

    FruitResponseDto create(FruitRequestDto fruitRequestDto);

    FruitResponseDto update(Long id,FruitRequestDto fruitRequestDto);

    void delete(Long id);

}
