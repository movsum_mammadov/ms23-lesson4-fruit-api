package az.ingress.ms23lesson4fruitapi.domain;

import jakarta.persistence.*;
import lombok.*;

@Data
@Entity
@Table(name = "fruits")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FruitEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String amount;

    private Double price;
}
