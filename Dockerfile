FROM alpine:3.18.3
RUN apk add --no-cache openjdk17
RUN apk add --no-cache tzdata
COPY build/libs/ms23-lesson4-fruit-api-1.0.33df4fe.jar /app/
WORKDIR /app/
ENTRYPOINT ["java"]
CMD ["-jar", "/app/ms23-lesson4-fruit-api-1.0.33df4fe.jar"]
